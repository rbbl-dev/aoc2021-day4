import * as fs from 'fs';

fs.readFile("input.txt", (err, data) => {
    if (err) {
        console.error(err)
        return
    }
    let reversedDataArray = splitByBlankLine(data.toString()).reverse()
    let input: number[] = reversedDataArray.pop()!.split(",").map(Number)
    let boards = buildBoards(reversedDataArray.reverse())
    console.log(getScore(input, boards))
})

function splitByBlankLine(data: string): string[] {
    return data.split(/\n\n/)
}

function buildBoards(data: string[]): number[][][] {
    let boards: number[][][] = []
    data.forEach(boardString => {
        boards.push(buildSingleBoard(boardString))
    })
    return boards
}

function buildSingleBoard(data: string): number[][] {
    let result: number[][] = []
    let rows = data.split("\n")
    rows.forEach(row => {
        let rowArray = row.trim().split(/\s+/).map(Number)
        result.push(rowArray)
    })
    return result
}

function buildMarkerBoards(boards: number[][][]): boolean[][][] {
    let markerBoards: boolean[][][] = []
    boards.forEach(board => {
        markerBoards.push(makeSingleMarkerBoard(board))
    })
    return markerBoards
}

function makeSingleMarkerBoard(board: number[][]): boolean[][] {
    let markerBoard: boolean[][] = []
    board.forEach(row => {
        let markerRow = row.map(() => Boolean(false))
        markerBoard.push(markerRow)
    })
    return markerBoard
}

function getScore(inputList: number[], boards: number[][][]): number {
    let markerBoards = buildMarkerBoards(boards)
    let winningBoardsIndices: number[] = []
    let winningInputs = []
    for (let input of inputList) {
        for (let listIterator = 0; listIterator < boards.length; listIterator++) {
            if (!winningBoardsIndices.includes(listIterator)) {
                for (let rowIterator = 0; rowIterator < boards[listIterator].length; rowIterator++) {
                    for (let itemIterator = 0; itemIterator < boards[listIterator][rowIterator].length; itemIterator++) {
                        if (boards[listIterator][rowIterator][itemIterator] == input) {
                            markerBoards[listIterator][rowIterator][itemIterator] = true
                        }
                    }
                }
            }
        }
        let indicesThatWon = getWinningIndices(markerBoards, winningBoardsIndices)
        if (indicesThatWon != undefined) {
            indicesThatWon.forEach(index => {
                winningBoardsIndices.push(index)
            })
            winningInputs.push(input)
            if (winningBoardsIndices.length == boards.length) {
                break
            }
        }
    }
    let lastWinningIndex = winningBoardsIndices.pop()!
    return calculateUnmarkedSum(boards[lastWinningIndex], markerBoards[lastWinningIndex]) * winningInputs.pop()!
}

function getWinningIndices(markerBoard: boolean[][][], indicesThatWon: number[]): number[] | undefined {
    let winningIndices = []
    for (let listIterator = 0; listIterator < markerBoard.length; listIterator++) {
        if (!indicesThatWon.includes(listIterator)) {
            if (checkColumnsForWinningCondition(markerBoard[listIterator]) || checkRowsForWinningCondition(markerBoard[listIterator])) {
                winningIndices.push(listIterator)
            }
        }
    }
    if(winningIndices.length==0) {
        return undefined
    }else {
        return winningIndices
    }
}

function checkRowsForWinningCondition(board: boolean[][]): boolean {
    for (let row of board) {
        if (arrayEquals(row, row.map(() => true))) {
            return true
        }
    }
    return false
}

function checkColumnsForWinningCondition(board: boolean[][]): boolean {
    for (let columnIterator = 0; columnIterator < board[0].length; columnIterator++) {
        let column: boolean[] = []
        board.forEach(row => {
            column.push(row[columnIterator])
        })
        if (arrayEquals(column, column.map(() => true))) {
            return true
        }
    }
    return false
}

function arrayEquals(a: any[], b: any[]) {
    return Array.isArray(a) &&
        Array.isArray(b) &&
        a.length === b.length &&
        a.every((val, index) => val === b[index]);
}

function calculateUnmarkedSum(board: number [][], markerBoard: boolean[][]): number {
    let sum = 0;
    for (let rowIterator = 0; rowIterator < markerBoard.length; rowIterator++) {
        for (let itemIterator = 0; itemIterator < markerBoard[rowIterator].length; itemIterator++) {
            if (!markerBoard[rowIterator][itemIterator]) {
                sum += board[rowIterator][itemIterator]
            }
        }
    }
    return sum
}